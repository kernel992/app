function main(){
    canvas = document.getElementById('canvas1');
    cv = canvas.getContext('2d');
    cv.fillStyle="#000000";
    cv.fillRect(0,0,400,400);
    keys();
}
function execlick(x,y) {
    if (x<200&&y<200){
        cv.strokeStyle="#ffffff";
        cv.beginPath();
        cv.moveTo(0,10);
        cv.lineTo(190,200);
        cv.closePath();
        cv.stroke();
    }
    if (x<200&&y>200){
        cv.fillStyle="#ff0000";
        cv.beginPath();
        cv.moveTo(100,220);
        cv.lineTo(180,380);
        cv.lineTo(20,380);
        cv.closePath();
        cv.fill();
    }
    if (x>200&&y<200){
        cv.fillStyle="#0000ff";
        cv.font = "20px sans-serif";
        cv.fillText("テスト",250,100,100);
    }
    if (x>200&&y>200){
        cv.fillStyle="#00ff00";
        cv.beginPath();
        cv.arc(300,300,80,0,Math.PI*2,false);
        cv.closePath();
        cv.fill();
    }
}
touchdev = false;
if (navigator.userAgent.indexOf('iPhone') > 0
    || navigator.userAgent.indexOf('iPod') > 0
    || navigator.userAgent.indexOf('iPad') > 0
    || navigator.userAgent.indexOf('Android') > 0) {
  touchdev = true;
}
function keys(){
    var lasttouchx,lasttouchy;
    if (touchdev == false) {
      canvas.addEventListener('click',clickfunc,false);
    } else {
      canvas.addEventListener("touchstart", touchstart, false);
      canvas.addEventListener("touchmove", touchmove, false);
      canvas.addEventListener("touchend", touchend, false);
    }
}
function clickfunc(event) {
  var rect = event.target.getBoundingClientRect();
  var x = event.clientX - rect.left;
  var y = event.clientY - rect.top;
  execlick(x,y);
}
function touchstart(event) {
  var rect = event.target.getBoundingClientRect();
  var x = event.touches[0].pageX - rect.left;
  var y = event.touches[0].pageY - rect.top;
  lasttouchx = x;
  lasttouchy = y;
}

function touchmove(event) {
  var rect = event.target.getBoundingClientRect();
  var x = event.touches[0].pageX - rect.left;
  var y = event.touches[0].pageY - rect.top;
  lasttouchx = x;
  lasttouchy = y;
}

function touchend(event) {
  execlick(lasttouchx,lasttouchy);
}
